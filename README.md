# Assets

Set of files for creating .fbx assets for various 3D applications, namely Unity and Unreal Engine. You can find an example scene of these assets in use [here](https://diaperand.deviantart.com/art/3D-Pony-Models-for-Unity-and-Unreal-Engine-684843362).

Model sources are stored as [Blender](https://www.blender.org/) files.

# Documentation

## How to Export Pony Model from Blender to .fbx (3D app compatible)

1. Import Pony .qc file
2. Remove VTA verticies
3. Append any Custom Pony Animations/Actions from Custom Pony Animations.blend
4. Make all texture names unique if you want auto-imported textures (wings->twilight_wings). Similar/same features don't need unique texture names (each ponies tongue, eyelashes, etc. are all same)
5. Open the Dope Sheet and rename the ragdoll action to 0_ragdoll, to make that the default animation
6. File->Export->FBX
7. Under 'Export FBX' -> Scale: 0.18 (I find this to be a good scale, but can be further adjusted in Unity/other apps)
8. Under 'Export FBX' -> Shift-deselect Empty, Camera, Lamp
9. Give file a name and click 'Export FBX'

## Import instructions (Unity)

1. Import .FBX file
2. Fix textures if necessary
3. Make sure to fix textures that are in the generated Materials folder, else they won't be reused on re-import

## Import instructions (Unreal Engine)

TODO

## Included Scripts (Unity)

**HeadLook**

Moves the pony's head towards a transform. You can specify the cutoff angle that they're allowed to turn (so they don't become owls).

**EyeFocus**

Offsets the pony's eye texture to point them towards a tracked object. **IMPORTANT:** This will modify the offset of the eye texture at runtime, and will affect all assets currently using that texture. Therefore if, for instance, you want to have two Twilights, they each need seperate left/right eye textures for their eyes to move independently.

**EyeBlinking**

Blinks the character's eye once every few seconds. A random value is added as an offset so that characters with the same script don't all blink at the same time.

## Animation

All pony animations are stored in the Pony Animation FBX. This separation was done to eliminate duplicated animations across all models. The animations from Pony Animation should work across all models as they have the same  bone structure.

**Note:** If you find some part of a pony's body becoming misaligned (the mane for instance), in Unity you can create an animation mask and uncheck the bones that are messing up. Applying this mask in your animation controller to the appropriate layer should cause the animation to not affect those bones, which should eliminate any weirdness. 

You can find an example of an animated pony in the unity package on the deviantart release page linked above.
