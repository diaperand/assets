Pony models adapted from Juicedane's Enchanced Pony Models:

https://juicedane.deviantart.com/

Eye tracking script based partially on scripts by XenoAisam:

https://xenoaisam.com/tutorial/

Original SFM->Blender->Unity tutorial by GreyGears:

https://greygears.tumblr.com
